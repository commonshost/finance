# finance 💸

*Be more transparent and accountable than necessary.*

## What?

Record financial transactions and subscriptions that cost money.

Record revenue earned from sale of servers, services, sponsorships, donations, etc.

## Why?

Allow anyone to fund confidently proposals.

Allow sharing of the administrative workload.

Offer information symmetry between the business & tech sides of the project.

## Background

Since the beginning Commons Host has relied on free tier services. Not much, if any, money was involved.

Deploying edge servers introduced a crowd-funded financial model. So far this has been managed by @sebdeckers.

Recent offers of sponsorships and increasing operational expenses suggest being more serious about finances would be helpful for the sustainability of the project.

Since everything about Commons Host is maximally transparent, finances should be the same.

## Role Models

- https://buffer.baremetrics.com
- https://buffer.com/transparency
